#!/usr/bin/env python
import math


def cointoss(wealth):
    """
    heads: you get 1,5 x your current wealth
    tails: you get 0.6 x
    """
    heads = 1.5 * wealth
    tails = 0.6 * wealth
    return heads, tails


def cointosses(wealths):
    ws = []
    for w in wealths:
        ws += cointoss(w)
    return ws


def histogram(values, i, ev):
    tlt1 = 0
    tge1 = 0
    h = {}
    for v in values:
        r = round(v, 2)
        if r in h:
            h[r] += 1
        else:
            h[r] = 1
    mx = max(h.values())
    scale = 1 if mx < 40 else 40.0/mx
    for v in sorted(h):
        print("%9.3f %s" % (v, "*" * int(math.ceil(scale * h[v]))))
        if v < 1:
            tlt1 += h[v]
        else:
            tge1 += h[v]
    meanval = sum(values)/len(values)
    print("%d < 1, %d >= 1\nmean value = %f\n%4.2f ** %d = %f\n" %
          (tlt1, tge1, meanval, ev, i, ev ** i))


iterations = 10
w = [1]
ev = sum(cointoss(1))/2
for i in range(iterations):
    w = cointosses(w)
    histogram(w, i+1, ev)
