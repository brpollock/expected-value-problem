# README #

See this [Twitter Thread](https://twitter.com/jonathanstray/status/1167217965638406144).

tldr:

- heads you win 50%, tails you lose 40%, expected value = (50-40)/2 = win 5%
- but, if you play more than once, it's sqrt(1.5*0.6) ~= lose 5% per turn

This code iterates several times to show what happens.

To see the latest output:

- click [Pipelines](https://bitbucket.org/brpollock/expected-value-problem/addon/pipelines/home), 
- select the most-recent successful pipeline
    - the top one
	- if it says 'Success' in the Status column
- click `python3 ev.py` to expand the output
